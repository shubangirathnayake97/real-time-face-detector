import cv2
from random import randrange

# load some pre trained data on face frontals from opencv (haar cascade algorithm)
trained_face_data = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')

#Choose image to detect face
# img = cv2.imread("rdj3.png")
web_cam = cv2.VideoCapture(0)

while True:

    successful_frame_read, frame = web_cam.read()

    #Convert image into gray
    gray_Scaled_Img = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY) 

    # detect faces
    face_coordinates = trained_face_data.detectMultiScale(gray_Scaled_Img)

    # Draw rectangle around the face
    for (x, y, w, h) in face_coordinates:
        cv2.rectangle(frame, (x, y),(x+w, y+h),(0, 255,0 ),5)

    
    cv2.imshow('Face Detector',frame)
    key = cv2.waitKey(1)
    if key == 113 or key == 81:
        break

# release the video capture
web_cam.release()

# Detect Coordinates
# face_coordinates = trained_face_data.detectMultiScale(gray_Scaled_Img)

# print(face_coordinates)

# Draw rectangle around the face
# for (x, y, w, h) in face_coordinates:
    # cv2.rectangle(img, (x, y),(x+w, y+h),(randrange(256), randrange(256), randrange(256)),2)

# Disply image
# cv2.imshow('Face Detector',img)

# Image is closing as soon as open.
# Show the image until any key pressed. 
# cv2.waitKey()

print("\nCode Completed.\n")